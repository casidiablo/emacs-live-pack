;; Load bindings config
(live-load-config-file "bindings.el")
(live-load-config-file "omm-writer.el")
(live-load-config-file "packaging.el")

(live-set-default-font "CosmicSansNeueMono 19")
(setq-default cursor-type '(bar . 1))

(setq org-directory "~/Dropbox/org/")
(setq org-agenda-files (list (concat org-directory "todo.org")))
(setq org-default-notes-file (concat org-directory "notes.org"))
(setq org-startup-indented t)
(setq org-archive-location (concat org-directory "archive.org::* From %s"))

(setq live-disable-zone t)
(projectile-global-mode)

(setq helm-github-stars-username "casidiablo")
