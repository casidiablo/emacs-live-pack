(live-add-pack-lib "emms")

(require 'emms-setup)

(setq omm-play-dir (file-truename (concat (live-pack-lib-dir) "../songs/")))

(defun omm-start ()
  "Start omm writter music"
  (interactive)
  (emms-standard)
  (emms-default-players)
  (emms-play-directory omm-play-dir)
  (emms-start))

(defun omm-pause ()
  "Pause current track"
  (interactive)
  (emms-pause))

(defun omm-stop ()
  "Stops all ommmmm"
  (interactive)
  (emms-stop))
